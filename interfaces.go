package csolve

type Puzzle interface {
	IsSolved() bool
	ApplyMove(int)
	ApplyMoves([]int)
	ValidMoves(int) []uint8
	Init()
}
