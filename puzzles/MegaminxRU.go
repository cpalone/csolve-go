package puzzles

type MegaminxRU struct {
	co [8]uint8
	cp [8]uint8
	eo [9]uint8
	ep [9]uint8
}

var IdentityMegaminxRU MegaminxRU = MegaminxRU{
	[8]uint8{0, 0, 0, 0, 0, 0, 0, 0},
	[8]uint8{0, 1, 2, 3, 4, 5, 6, 7},
	[9]uint8{0, 0, 0, 0, 0, 0, 0, 0, 0},
	[9]uint8{0, 1, 2, 3, 4, 5, 6, 7, 8}}
var MovesMegaminxRU [8]MegaminxRU

func (m *MegaminxRU) Init() {
	MovesMegaminxRU[0] = MegaminxRU{
		[8]uint8{0, 0, 0, 0, 0, 0, 0, 0},
		[8]uint8{4, 0, 1, 2, 3, 5, 6, 7},
		[9]uint8{0, 0, 0, 0, 0, 0, 0, 0, 0},
		[9]uint8{4, 0, 1, 2, 3, 5, 6, 7, 8}}
	MovesMegaminxRU[1] = Multiply(&MovesMegaminxRU[0], &MovesMegaminxRU[0])
	MovesMegaminxRU[2] = Multiply(&MovesMegaminxRU[1], &MovesMegaminxRU[0])
	MovesMegaminxRU[3] = Multiply(&MovesMegaminxRU[2], &MovesMegaminxRU[0])
	MovesMegaminxRU[4] = MegaminxRU{
		[8]uint8{0, 0, 1, 0, 0, 0, 0, 2},
		[8]uint8{0, 1, 3, 7, 4, 2, 5, 6},
		[9]uint8{0, 0, 0, 0, 0, 0, 0, 0, 0},
		[9]uint8{0, 1, 8, 3, 4, 2, 5, 6, 7}}
	MovesMegaminxRU[5] = Multiply(&MovesMegaminxRU[4], &MovesMegaminxRU[4])
	MovesMegaminxRU[6] = Multiply(&MovesMegaminxRU[5], &MovesMegaminxRU[4])
	MovesMegaminxRU[7] = Multiply(&MovesMegaminxRU[6], &MovesMegaminxRU[4])
}

func (m *MegaminxRU) ValidMoves(prevMove int) []uint8 {
	if prevMove > 3 {
		return []uint8{0, 1, 2, 3}
	} else if prevMove >= 0 {
		return []uint8{4, 5, 6, 7}
	} else {
		return []uint8{0, 1, 2, 3, 4, 5, 6, 7}
	}
}

func (m *MegaminxRU) IsSolved() bool {
	for i := 0; i < 8; i++ {
		if m.co[i] != 0 || m.cp[i] != uint8(i) {
			return false
		}
	}
	for i := 0; i < 9; i++ {
		if m.eo[i] != 0 || m.ep[i] != uint8(i) {
			return false
		}
	}
	return true
}

func (m *MegaminxRU) ApplyMegaminxRU(to_apply *MegaminxRU) {
	var new_eo, new_ep [9]uint8
	var new_co, new_cp [8]uint8
	for i := 0; i < 8; i++ {
		new_cp[i] = m.cp[to_apply.cp[i]]
		new_co[i] = (m.co[to_apply.cp[i]] + to_apply.co[i]) % 3
	}
	for i := 0; i < 9; i++ {
		new_ep[i] = m.ep[to_apply.ep[i]]
		new_eo[i] = (m.eo[to_apply.ep[i]] + to_apply.eo[i]) % 2
	}
	m.eo = new_eo
	m.ep = new_ep
	m.co = new_co
	m.cp = new_cp
}

func (m *MegaminxRU) ApplyMove(move int) {
	m.ApplyMegaminxRU(MovesMegaminxRU[i])
}

func (m *MegaminxRU) ApplyMoves(moves []int) {
	for _, move := range moves {
		m.ApplyMove(move)
	}
}

func Multiply(a *MegaminxRU, b *MegaminxRU) MegaminxRU {
	var new_eo, new_ep [9]uint8
	var new_co, new_cp [8]uint8
	var ab MegaminxRU
	for i := 0; i < 8; i++ {
		new_cp[i] = a.cp[b.cp[i]]
		new_co[i] = (a.co[b.cp[i]] + b.co[i]) % 3
	}
	for i := 0; i < 9; i++ {
		new_ep[i] = a.ep[b.ep[i]]
		new_eo[i] = (a.eo[b.ep[i]] + b.eo[i]) % 2
	}
	ab.eo = new_eo
	ab.ep = new_ep
	ab.co = new_co
	ab.cp = new_cp
	return ab
}
