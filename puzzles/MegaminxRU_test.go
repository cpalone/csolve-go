package puzzles

import (
	"testing"
)

func sliceEquals(a []uint8, b []uint8) bool {
	if len(a) != len(b) {
		return false
	} else {
		for i := 0; i < len(a); i++ {
			if a[i] != b[i] {
				return false
			}
		}
		return true
	}
}

func TestMegaminxRUIsSolved(t *testing.T) {
	if !IdentityMegaminxRU.IsSolved() || MovesMegaminxRU[0].IsSolved() {
		t.Error("MegaminxRU has an incorrect implementation of IsSolved.")
	}
}

func TestMegaminxRUValidMoves(t *testing.T) {
	m := IdentityMegaminxRU.ValidMoves(0)
	if !sliceEquals(m, []uint8{4, 5, 6, 7}) {
		t.Error("MegaminxRU.ValidMoves(0) fails.")
	}
	m = IdentityMegaminxRU.ValidMoves(3)
	if !sliceEquals(m, []uint8{4, 5, 6, 7}) {
		t.Error("MegaminxRU.ValidMoves(3) fails.")
	}
	m = IdentityMegaminxRU.ValidMoves(4)
	if !sliceEquals(m, []uint8{0, 1, 2, 3}) {
		t.Error("MegaminxRU.ValidMoves(4) fails.")
	}
	m = IdentityMegaminxRU.ValidMoves(7)
	if !sliceEquals(m, []uint8{0, 1, 2, 3}) {
		t.Error("MegaminxRU.ValidMoves(7) fails.")
	}
	m = IdentityMegaminxRU.ValidMoves(-1)
	if !sliceEquals(m, []uint8{0, 1, 2, 3, 4, 5, 6, 7}) {
		t.Error("MegaminxRU.ValidMoves(-1) fails.")

	}
}
